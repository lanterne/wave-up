package com.jarsilio.android.waveup

import android.content.Context
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.BuildConfig
import androidx.multidex.MultiDexApplication
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import com.jarsilio.android.waveup.receivers.ServiceTogglerReceiver
import org.acra.ACRA
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraMailSender
import org.acra.annotation.AcraNotification
import timber.log.Timber

@AcraCore(buildConfigClass = BuildConfig::class)

@AcraMailSender(mailTo = "juam+waveup@posteo.net")

@AcraNotification(
        resTitle = R.string.acra_notification_title,
        resText = R.string.acra_notification_text,
        resChannelName = R.string.acra_notification_channel_name,
        resSendButtonText = R.string.acra_notification_send,
        resDiscardButtonText = android.R.string.cancel,
        resSendButtonIcon = R.drawable.email_icon_gray,
        resDiscardButtonIcon = R.drawable.cancel_icon_gray
)

@Suppress("unused")
class WaveUpApplication : MultiDexApplication() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

        if (!BuildConfig.DEBUG) {
        // The following line triggers the initialization of ACRA
            ACRA.init(this)
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))

        Timber.d("Registering ServiceToggler BroadcastReceiver for com.jarsilio.android.waveup.action.WAVEUP_ENABLE")
        val filter = IntentFilter("com.jarsilio.android.waveup.action.WAVEUP_ENABLE")

        val receiver = ServiceTogglerReceiver()
        registerReceiver(receiver, filter)

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }
}
