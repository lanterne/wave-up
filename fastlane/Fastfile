# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

import "../libcommon/fastlane/Fastfile"

gitlab_project_id = "747995"

default_platform(:android)

platform :android do

  lane :build do
    build_flavor(flavor: "standard")
  end

  lane :build_all do
    build_flavor(flavor: "")
  end

  lane :build_standard do
    build_flavor(flavor: "standard")
  end

  lane :build_fortune_cookies do
    build_flavor(flavor: "fortunecookies")
  end

  desc "Deploy production (all flavors) to the Google Play"
  lane :deploy_all do
    deploy_standard
    deploy_fortune_cookies
  end

  desc "Deploy production (paid flavors) to the Google Play"
  lane :deploy_paid do
    deploy_fortune_cookies
  end

  desc "Deploy production 'standard' flavor to the Google Play"
  lane :deploy_standard do
    deploy_flavor(flavor: "standard")
  end

  desc "Deploy production 'fortune_cookies' flavor to the Google Play"
  lane :deploy_fortune_cookies do
    deploy_flavor(flavor: "fortunecookies")
  end

  lane :update_metadata_all do
    update_metadata_standard
    update_metadata_fortune_cookies
  end

  lane :update_metadata_standard do
    update_metadata_flavor(flavor: "standard")
  end

  lane :update_metadata_fortune_cookies do
    update_metadata_flavor(flavor: "fortunecookies")
  end

  desc "Fetch all metadata of apps"
  lane :fetch_metadata do
    update_metadata_flavor(flavor: "standard")
    update_metadata_flavor(flavor: "fortunecookies")
  end

  lane :gitlab_release do |values|# only release standard release for now
    gitlab_release_for_project_id(project_id: gitlab_project_id, check_version: values[:check_version])
  end

end
