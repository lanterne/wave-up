New in 3.0.8
★ Fix crash on some devices that can't write log to cache dir.
★ Possibly fix problem while attaching logs to send with email app.

New in 3.0.7
★ Fix small error reporting bugs.

New in 3.0.6
★ Add menu item to send debug logs to dev
★ If for some reason the device admin or accessibility service are disabled, show a dialog while reopening the app.
★ (Only paid apps) Show a dialog asking the user *not* to uninstall the app immediately (unless s/he wants a refund).
