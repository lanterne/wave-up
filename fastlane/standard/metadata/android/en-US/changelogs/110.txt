New in 3.2.6
★ Add Galician translation.
★ Fix 'Privacy Policy' title colour.

New in 3.2.5
★ Remove "send debug logs to dev" option.

New in 3.2.4
★ Update some translations

New in 3.2.3
★ Update some translations
★ pdate privacy policy (add ACRA section).

New in 3.2.2
★ Fix bug while trying to report a bug :)

New in 3.2.1
★ Update some translations.
